docker-and-compose(1)
=====================

builds off of `docker:1.12.6` and adds `docker-compose==1.9.0`, `triton`, `triton-docker`, and `triton-compose`

## usage

```yaml
# .gitlab-ci.yml
image: registry.gitlab.com/citygro/docker-and-compose
# ...
```

## notes

https://docs.gitlab.com/ee/ci/ssh_keys/README.html
https://docs.joyent.com/public-cloud/api-access/cloudapi
https://docs.joyent.com/public-cloud/network/cns
